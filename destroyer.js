function waitForElementToDisplay(selector, callback, checkFrequencyInMs, timeoutInMs) {
    var startTimeInMs = Date.now();
    (function loopSearch() {
        if (document.querySelector(selector) != null) {
            callback();
            return;
        }
        else {
            setTimeout(function () {
                if (timeoutInMs && Date.now() - startTimeInMs > timeoutInMs)
                    return;
                loopSearch();
            }, checkFrequencyInMs);
        }
    })();
}

function adjustVideo(vid) {
    vid.removeAttribute("style");
    vid.removeAttribute("preload");
    vid.setAttributeNode(document.createAttribute("controls"));
    vid.id = "plyr";
    return vid;
}

function destroyPaella() {
    let vid = adjustVideo(document.getElementById("video_0"));
    document.body.innerHTML = "";
    document.body.appendChild(vid);
    Plyr.setup("#plyr");
    console.log("Setup plyr");
}
if (!window.hasRun) {
    document.head.insertAdjacentHTML("beforeend", `<style>${style}</style>`);
    window.hasRun = false;
}
console.log("Waiting for video_0");
waitForElementToDisplay("#video_0", destroyPaella, 500, 3000);