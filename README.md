# Paella Killer
> Firefox add-on that replaces the unhandy paella player used by the kit's ilias with a better working video player
## ✨ Demo
As soon as you enter a `ilias.studium.kit.edu` page this add-on replaces the paella video element `video_0` with the [Plyr](https://plyr.io/) video player. 
- playback speed up to 4x possible (instead of 1.5x)
- no more playback speed reset after toggeling fullscreen
- unfortunately, the player's icons cannot be loaded at the moment
<p align="center">
  <img width="700" src="images/demo.png"/>
</p>

## 🚀 Installation
1. Go to the [versions folder](./versions),  download the latest version
2. Open firefox and navigate to, _addons_ > _Install Add-on From File_ and choose the downloaded `.xpi` file. 